#pragma checksum "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9bcaf385151bb1a326931c71dcbd19e3d7ef308c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\_ViewImports.cshtml"
using CPSAPP.UI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\_ViewImports.cshtml"
using CPSAPP.UI.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9bcaf385151bb1a326931c71dcbd19e3d7ef308c", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5d6692e5ed9657aa8426105e61753ff002d759c1", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CPSAPP.UI.Models.CustomerOrderModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";
    var error = TempData["Error"] as string;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<style>
    .grid-divider {
        margin-top: 15px;
    }

    .cardlimit {
        overflow: auto;
        height: 500px;
    }

    .alignPrint {
        padding-right: 0px;
        padding-bottom: 15px;
    }
</style>

<div class=""text-center"">

");
#nullable restore
#line 26 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
     if (!string.IsNullOrEmpty(error))
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">\r\n            <strong>Error: ");
#nullable restore
#line 29 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                      Write(error);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n                <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n        </div>\r\n");
#nullable restore
#line 34 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 36 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
     using (Html.BeginForm("Index", "Home", FormMethod.Post, new { enctype = "multipart/form-data" }))
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"col-md-6 offset-md-6\">\r\n            <div class=\"form-row\">\r\n                <div class=\"col-md-6\">\r\n                    ");
#nullable restore
#line 41 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
               Write(Html.TextBoxFor(m => m.UploadFile, new { type = "file", @class = "form-control-file" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n\r\n                <div class=\"col-md-6\">\r\n                    <button id=\"upload-button\" class=\"btn btn-secondary form-control\" type=\"submit\">Upload Record</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n");
#nullable restore
#line 49 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"

    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"

    <div class=""grid-divider"">

        <div class=""row"">
            <div class=""col-md-12"">
                <div class=""card"">
                    <div class=""card-header"">
                        Customer Order Details
                    </div>
                    <div class=""card-body cardlimit"">
");
#nullable restore
#line 62 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                         if (Model.IsFileValide)
                        {


#line default
#line hidden
#nullable disable
            WriteLiteral("                            <div");
            BeginWriteAttribute("style", " style=\"", 1805, "\"", 1813, 0);
            EndWriteAttribute();
            WriteLiteral(@" class=""col-md-2 offset-md-10 alignPrint"">
                                <button id=""search-button"" class=""btn btn-secondary form-control"" 
                                onclick=PrintOrder(""customerOrder"");>Print Record</button>
                            </div>
                            <table id=""customerOrder"" border=""1"" cellpadding=""2"" class=""table table-striped"">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>CustomerName</th>
                                        <th>CustomerEmail</th>
                                        <th>Quantity</th>
                                        <th>Size</th>
                                        <th>DateRequired</th>
                                        <th>Note</th>
                                    </tr>
                                </thead>
                                <tbody>
");
#nullable restore
#line 82 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                      
                                        int count = 1;
                                        foreach (var order in Model.CustomerOrder)
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <tr>\r\n                                                <td>");
#nullable restore
#line 87 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                               Write(count);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                                <td>");
#nullable restore
#line 88 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                               Write(order.CustomerName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                                <td>");
#nullable restore
#line 89 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                               Write(order.CustomerEmail);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </td>\r\n                                                <td>");
#nullable restore
#line 90 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                               Write(order.Quantity);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                                <td>");
#nullable restore
#line 91 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                               Write(order.Size);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                                <td>");
#nullable restore
#line 92 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                               Write(order.DateRequired);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                                <td>");
#nullable restore
#line 93 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                               Write(order.Notes);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                            </tr>\r\n");
#nullable restore
#line 95 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"

                                            count++;
                                        }
                                    

#line default
#line hidden
#nullable disable
            WriteLiteral("                                </tbody>\r\n                            </table>\r\n");
#nullable restore
#line 101 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 103 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                         if (!Model.IsFileValide)
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                            <table class=""table table-striped"">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>OrderDate</th>
                                        <th>Error</th>
                                    </tr>
                                </thead>
                                <tbody>
");
#nullable restore
#line 114 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                      
                                        int count = 1;
                                        foreach (var order in Model.CustomerOrder.Where(x => x.errorList.Count > 0))
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <tr>\r\n                                                <td>");
#nullable restore
#line 119 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                               Write(count);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                                <td>\r\n                                                    <span>Customer Name: ");
#nullable restore
#line 121 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                                                    Write(order.CustomerName);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </span><br />\r\n                                                    <span>Customer Email: ");
#nullable restore
#line 122 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                                                     Write(order.CustomerEmail);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </span><br />\r\n                                                    <span>Date of Order: ");
#nullable restore
#line 123 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                                                    Write(order.DateRequired);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </span><br />\r\n                                                    <span>Size: ");
#nullable restore
#line 124 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                                           Write(order.Size);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </span><br />\r\n                                                    <span>Quantity: ");
#nullable restore
#line 125 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                                               Write(order.Quantity);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </span><br />\r\n                                                </td>\r\n                                                <td>\r\n");
#nullable restore
#line 128 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                                      
                                                        foreach (var msg in order.errorList)
                                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                            <span> ");
#nullable restore
#line 131 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                                              Write(msg.ToString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n");
#nullable restore
#line 132 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"

                                                        }
                                                    

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                </td>\r\n                                            </tr>\r\n");
#nullable restore
#line 137 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                                            count++;
                                        }
                                    

#line default
#line hidden
#nullable disable
            WriteLiteral("                                </tbody>\r\n                            </table>\r\n");
#nullable restore
#line 142 "C:\Users\Lisa\source\repos\CPSAPP.UI\CPSAPP.UI\Views\Home\Index.cshtml"
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CPSAPP.UI.Models.CustomerOrderModel> Html { get; private set; }
    }
}
#pragma warning restore 1591

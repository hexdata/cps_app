﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPSAPP.Core.Models;

namespace CPSAPP.UI.Models
{
    public class CustomerOrderModel
    {
        public IFormFile UploadFile { get; set; }
        public List<Order> CustomerOrder { get; set; }   = new List<Order>();
        public bool IsFileValide { get; set; } = false;
        
    }
}

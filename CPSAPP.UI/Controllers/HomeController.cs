﻿using CPSAPP.Infrastructure;
using CPSAPP.Core.Interface;
using CPSAPP.UI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace CPSAPP.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IProcessor _processor;

        public HomeController(ILogger<HomeController> logger, IProcessor processor )
        {
            _logger = logger;
            _processor = processor;
        }

        public IActionResult Index()
        {
            return View(new CustomerOrderModel());
        }

        [HttpPost]
        public async Task<IActionResult> Index(IFormFile UploadFile)
        {


            try
            {

                if (UploadFile  == null)
                {
                    TempData["Error"] = $"Kindly select a file to upload... ";
                    return View(new CustomerOrderModel() { IsFileValide = false });
                }

                var order = await Helper.Utility.GetDateFromFile(UploadFile);

                var data = Processor.GetDataFromXml(order);

                _processor.Set(data);

                var processedOrders = _processor.Get();

                var model = new CustomerOrderModel()
                {
                    IsFileValide = !(processedOrders.Sum(x => x.errorList.Count) > 0),
                    CustomerOrder = processedOrders
                };
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["Error"] = $"{ex.Message}... ";
                _logger.LogError(ex.StackTrace);
                return View(new CustomerOrderModel() { IsFileValide = false });
            }

        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

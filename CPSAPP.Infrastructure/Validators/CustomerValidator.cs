﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;
using CPSAPP.Core.Constants;

namespace CPSAPP.Infrastructure
{
    public class CustomerValidator
    {

        public bool IsEmailValid(string email)
        {
            //return new EmailAddressAttribute().IsValid(email);
            return Regex.IsMatch(email, Constant.EmailRegex, RegexOptions.IgnoreCase);
        }

        public bool IsRequired(string value)
        {
            return new RequiredAttribute().IsValid(value);
        }

        public bool IsDateValid(string date, out DateTime real_date)
        {
            //2021-05-04 ===>> YYYY-MM-DD
            //fix culture
            //CultureInfo culture = CultureInfo.CreateSpecificCulture("nl-NL");
            //return DateTime.TryParse(date, culture, DateTimeStyles.None, out real_date);
            return DateTime.TryParse(date, out real_date);
        }
        public bool IsDateInRange(DateTime date)
        {
            return date.Subtract(DateTime.Now).Days >= 10;
        }

        public bool IsRangeValid(Type type, string min, string max, object value)
        {
            return new RangeAttribute(type, min, max).IsValid(value);
        }

        public bool IsSizeValid(decimal size)
        {
            return new RangeAttribute(typeof(decimal), "11.5", "15").IsValid(size);
        }

        public bool IsSizeValidIfDecimal(decimal size)
        {
            return  size % 0.5M == 0;
        }

        public bool IsSizeDecimal(decimal size)
        {
            return !(size % 1 == 0);
        }

        public bool IsQuantityValid(int quantity)
        {
            return quantity % 1000 == 0;
        }

    }
}

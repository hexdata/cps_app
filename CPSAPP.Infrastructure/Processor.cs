﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using CPSAPP.Core.Models;
using CPSAPP.Core.Constants;
using CPSAPP.Core.Interface;

namespace CPSAPP.Infrastructure
{
    public class Processor : IProcessor
    {
        private BigShoeDataImport _bigShoeDataImport;
        private CustomerValidator _customerValidator;
        public Processor()
        {

        }

        public void Set(BigShoeDataImport bigShoeDataImport)
        {
            _bigShoeDataImport = bigShoeDataImport;
            _customerValidator = new CustomerValidator();
        }

        public List<Order> Get()
        {
            return ValidateFields();
        }


        private List<Order> ValidateFields()
        {
            try
            {
                var orders = new List<Order>();
                var  errorList = string.Empty;
                foreach (var order in _bigShoeDataImport.Order)
                {
                    var newOrder = new Order();
                    newOrder.CustomerName = ValidateCustomerNameField(order.CustomerName, out errorList);
                     if (!string.IsNullOrEmpty(errorList)) newOrder.errorList.Add(errorList);

                    newOrder.CustomerEmail = ValidateCustomerEmailField(order.CustomerEmail, out errorList);
                    if (!string.IsNullOrEmpty(errorList)) newOrder.errorList.Add(errorList);

                    newOrder.Quantity = ValidateQuantityField(order.Quantity, out errorList);
                    if (!string.IsNullOrEmpty(errorList)) newOrder.errorList.Add(errorList);

                    newOrder.Size = ValidateSizeField(order.Size, out errorList);
                    if (!string.IsNullOrEmpty(errorList)) newOrder.errorList.Add(errorList);

                    newOrder.DateRequired = ValidateDateField(order.DateRequired, out errorList);
                    if (!string.IsNullOrEmpty(errorList)) newOrder.errorList.Add(errorList);

                    newOrder.Notes = order.Notes;
                    orders.Add(newOrder);
                }
                return orders;
            }
            catch (Exception ex)
            {
                throw ex;
                // or log in file using log4net,nlog etc
            }
        }

        private string ValidateCustomerNameField(string name, out string error)
        {
            if (!_customerValidator.IsRequired(name))
            {
                error = "Customer Name is a Required Field";
                return name;
            }
            error = string.Empty;
            return name;
        }

        private string ValidateCustomerEmailField(string email, out string error)
        {

            if (!_customerValidator.IsRequired(email))
            {
                error = "Customer Email is a Required Field";
                return email;
            }

            if (!_customerValidator.IsEmailValid(email))
            {
                error = "Customer Email is not valid";
                return email;
            }
            error = string.Empty;
            return email;
        }
        private int ValidateQuantityField(int quantity, out string error)
        {
            if (!_customerValidator.IsRequired(quantity.ToString()))
            {
                error = "Order Quantity is a Required Field";
                return quantity;
            }

            if (!_customerValidator.IsQuantityValid(quantity))
            {
                error = "Order Quantity must be in multiples of 1000";
                return quantity;
            }
            error = string.Empty;
            return quantity;
        }

        private float ValidateSizeField(decimal size, out string error)
        {

            if (!_customerValidator.IsSizeValid(size))
            {
                error = $"Size must be in the range of {Constant.MinSize } to { Constant.MaxSize }";
                return (float)size;
            }

            if (_customerValidator.IsSizeDecimal(size))
            {
                if (!_customerValidator.IsSizeValidIfDecimal(size))
                {
                    error = $"Size must be of the following values { Constant.ValidDecimals}";
                    return (float)size;
                }
            }
            error = string.Empty;
            return (float)size;
        }

        private DateTime ValidateDateField(DateTime date, out string error)
        {

            if (!_customerValidator.IsDateInRange(date))
            {
                error = "The Date field is not valid it must be atleast 10 days from today";
                return date;
            }
            error = string.Empty;
            return date;
        }

        public static BigShoeDataImport GetDataFromXml(String xml)
        {
            var xmlObject = new Core.Models.BigShoeDataImport();

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        xmlObject = (Core.Models.BigShoeDataImport)new XmlSerializer(typeof(Core.Models.BigShoeDataImport)).Deserialize(reader);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("The Customer Order Data File (.xml) is not valid");
                        //throw ex;
                        // or log in file using log4net,nlog etc
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("The Customer Order Data File (.xml) is not valid");
                //throw ex;
                // or log in file using log4net,nlog etc
            }

            return xmlObject;
        }

    }
}

﻿using CPSAPP.Infrastructure;
using System;
using Xunit;

namespace CPSAPP.Test
{
    public class ProcessorTest
    {

        [Fact]
        public void ShouldGetAllUrlfromSiteData()
        {
            string customerData = System.IO.File.ReadAllText($@"..\..\..\Data\test.xml"); 
            string expected_0 = "test@test.com";
            string expected_1 = "test2@test2.com";

            var data = Infrastructure.Processor.GetDataFromXml(customerData);

            Assert.NotNull(data);
            Assert.Equal(expected_0, data.Order[0].CustomerEmail);
            Assert.Equal(expected_1, data.Order[1].CustomerEmail);

        }
    }
}

using CPSAPP.Infrastructure;
using System;
using Xunit;

namespace CPSAPP.Test
{
    public class CustomerValidatorTest
    {
        [Theory]
        [InlineData("mark.wheeler@cps.world", true)]
        [InlineData("uchehens@yahoo.com", true)]
        [InlineData("man@gmail.", false)]
        [InlineData("root.com", false)]
        [InlineData("root@com", false)]
        [InlineData("root@.gmail.com", false)]
        [InlineData("world", false)]
        public void ShouldValidateCustomerEmail(string email, bool expected)
        {
            var validator = new CustomerValidator();

            var result = validator.IsEmailValid(email);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("hello", true)]
        [InlineData("world", true)]
        [InlineData("", false)]
        [InlineData("+", true)]
        [InlineData("h", true)]
        public void ShouldValidateIsRequired(string value, bool expected)
        {
            var validator = new CustomerValidator();

            var result = validator.IsRequired(value);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("2021-05-04", true)]
        [InlineData("2021-05-40", false)]
        [InlineData("2021-13-06", false)]
        [InlineData("2021-12-12", true)]
        public void ShouldValidateDateAndRetriveValue(string date, bool expected)
        {
            var validator = new CustomerValidator();

            DateTime value; 
            var result = validator.IsDateValid(date, out value);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("2021-09-22", false)]
        [InlineData("2021-10-01", false)]
        [InlineData("2021-10-02", false)]
        [InlineData("2021-10-03", false)]
        [InlineData("2021-10-04", true)]
        [InlineData("2021-10-05", true)]
        [InlineData("2021-10-06", true)]
        [InlineData("2021-10-07", true)]
        [InlineData("2021-10-08", true)]
        [InlineData("2021-10-09", true)]
        public void ShouldValidateDateInRange(string date, bool expected)
        {
            var validator = new CustomerValidator();
            DateTime value;

            validator.IsDateValid(date, out value);

            var result = validator.IsDateInRange(value);
       
            Assert.Equal(expected, result);
        }


        [Theory]
        [InlineData(9, false)]
        [InlineData(10.5, false)]
        [InlineData(10.59, false)]
        [InlineData(11.5, true)]
        [InlineData(12, true)]
        [InlineData(12.5, true)]
        [InlineData(13, true)]
        [InlineData(13.5, true)]
        [InlineData(14, true)]
        [InlineData(14.5, true)]
        [InlineData(15, true)]
        [InlineData(16, false)]
        public void ShouldBeValidateSizeRange(decimal size, bool expected)
        {
            var validator = new CustomerValidator();

            var result = validator.IsSizeValid(size);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(11.4, false)]
        [InlineData(11.5, true)]
        [InlineData(11.51, false)]
        [InlineData(12.5, true)]
        [InlineData(12.15, false)]
        [InlineData(12.95, false)]
        [InlineData(13.5, true)]
        [InlineData(13.25, false)]
        [InlineData(13.35, false)]
        [InlineData(14.55, false)]
        [InlineData(14.5, true)]
        public void SizeShouldBeValideRangeIfDecimal(decimal size, bool expected)
        {
            var validator = new CustomerValidator();

            var result = validator.IsSizeValidIfDecimal(size);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(11, false)]
        [InlineData(11.5, true)]
        [InlineData(11.51, true)]
        [InlineData(12, false)]
        [InlineData(12.15, true)]
        [InlineData(12.95, true)]
        [InlineData(13, false)]
        [InlineData(13.35, true)]
        [InlineData(14.55, true)]
        [InlineData(14, false)]
        public void ShouldValidateIsSizeInDecimal(decimal size, bool expected)
        {
            var validator = new CustomerValidator();

            var result = validator.IsSizeDecimal(size);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(100, false)]
        [InlineData(999, false)]
        [InlineData(1000, true)]
        [InlineData(1500, false)]
        [InlineData(2000, true)]
        [InlineData(2500, false)]
        [InlineData(1001, false)]
        [InlineData(5000, true)]
        [InlineData(100000, true)]
        [InlineData(3000000, true)]
        [InlineData(300000000, true)]
        [InlineData(999999999, false)]
        public void ShouldValidateIfQuantityIsInRange(int quantity, bool expected)
        {
            var validator = new CustomerValidator();

            var result = validator.IsQuantityValid(quantity);

            Assert.Equal(expected, result);
        }

    }
}

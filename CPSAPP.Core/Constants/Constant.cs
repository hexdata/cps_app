﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CPSAPP.Core.Constants
{
    public static class Constant
    {

        public const string EmailRegex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

        public const string MinSize = "11.5";
        public const string MaxSize = "15";
        public const string ValidDecimals = "11.5, 12.5 ,13.5 ,14.5 ";
    }
}

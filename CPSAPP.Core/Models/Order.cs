﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CPSAPP.Core.Models
{
   public class Order
    {
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public int Quantity { get; set; }
        public string Notes { get; set; }
        public float Size { get; set; }
        public DateTime DateRequired { get; set; }

        public List<string> errorList = new List<string>();
    }
}

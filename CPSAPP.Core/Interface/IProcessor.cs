﻿using CPSAPP.Core.Models;
using System.Collections.Generic;

namespace CPSAPP.Core.Interface
{
    public interface IProcessor
    {
        void Set(BigShoeDataImport bigShoeDataImport);
        List<Order> Get();
    }
}